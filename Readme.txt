**********************
*** How to Compile ***
**********************

- In a terminal cd into the project root folder sudokuCSP.

- If the directory 'bin' does not exist at this location, create it with the following command:
mkdir bin

- Enter the following command to compile:
javac -d bin -sourcepath src src/sudoku/solver/Main.java



*******************************
*** Running the Application ***
*******************************

- In a terminal, navigate to the project root folder sudokuCSP.

- Enter the following command to change to the 'bin' subdirectory.
cd bin

- Enter the following command to begin program execution.
java sudoku.solver.Main

- On screen prompts will give instructions on program input. I've also included a testInputs.txt file
- containing an example set of program inputs. Larger board (such as the 16x16 example) can take a
- few minutes.