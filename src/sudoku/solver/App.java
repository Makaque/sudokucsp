package sudoku.solver;

import sudoku.solver.constraint.satisfaction.DomainOrderer;
import sudoku.solver.constraint.satisfaction.VariableSelector;
import sudoku.solver.game.*;
import sudoku.solver.io.IO;
import sudoku.solver.types.MultiPair;
import sudoku.solver.utils.IOMessage;

import java.util.Optional;

public class App {

    public App() {
    }

    private MultiPair<Sudoku, Boolean> backTrack(
            VariableSelector<SudokuVariable, Integer> selector, DomainOrderer<SudokuDomain,
            SudokuVariable, Integer> orderer, Sudoku sudoku) {
        SudokuBacktrack backtrack = new SudokuBacktrack();
        SudokuCSP sudokuCSP = new SudokuCSP(selector, orderer, sudoku);
        Optional<Sudoku> maybeSolved = backtrack.backtrack(sudoku, sudokuCSP);
        if (maybeSolved.isPresent()) {
            return new MultiPair<>(maybeSolved.get(), true);
        }
        return new MultiPair<>(sudoku, false);
    }

    public void run(IO io, VariableSelector<SudokuVariable, Integer> selector, DomainOrderer<SudokuDomain, SudokuVariable, Integer> orderer) {
        IOMessage<MultiPair<Sudoku, Boolean>> result = new IOMessage.Success<>(null);
        while (result.isSuccess()) {
            io.displayDirections();

            IOMessage<Sudoku> sudokuIOMessage =
                    io.promptBoardWidth().flatMap(widthMsg ->
                            io.promptBoardValues(widthMsg.payload)
                    );


            result =
                    sudokuIOMessage.map(sMsg -> backTrack(selector, orderer, sMsg.payload));

            result.forEach(res ->
                    io.reportResult(res.payload)
            );
            if (result.isError()) {
                io.reportError((IOMessage.Error<?>) result);
                result = new IOMessage.Success<>(null);
            } else if (result.isQuit()) {
                io.reportQuit();
            }
        }
    }

}
