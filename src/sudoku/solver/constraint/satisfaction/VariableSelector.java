package sudoku.solver.constraint.satisfaction;

import java.util.List;
import java.util.Optional;

/**
 * Created on 19/03/2018.
 */
public interface VariableSelector<T extends CSPVariable, U>
{
    Optional<T> selectUnassignedVariable(List<T> vars, Assignment<T,U> assignment);
}
