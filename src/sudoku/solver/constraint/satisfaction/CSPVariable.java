package sudoku.solver.constraint.satisfaction;

/**
 * Created on 19/03/2018.
 */
public interface CSPVariable {
    CSPDomain getDomain();
}
