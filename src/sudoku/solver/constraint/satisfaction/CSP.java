package sudoku.solver.constraint.satisfaction;

import java.util.List;
import java.util.Optional;

/**
 * Created on 19/03/2018.
 */
public interface CSP <R extends CSPVariable, L, A extends Assignment<R,L>> {


    Optional<R> selectUnassignedVariable(A assignment);

    List<L> orderDomainValues(R var, A assignment);

    boolean consistent(R var, L value, A assignment);

    CSP<R,L,A> apply(R var, L value, A assignment);
}
