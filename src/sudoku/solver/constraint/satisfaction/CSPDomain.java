package sudoku.solver.constraint.satisfaction;

import java.util.List;

/**
 * Created on 19/03/2018.
 */
public interface CSPDomain<T> {
    int size();

    List<T> getValues();
}
