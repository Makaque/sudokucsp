package sudoku.solver.constraint.satisfaction;

/**
 * Created on 19/03/2018.
 */
public interface DomainOrderer<T extends CSPDomain, R extends CSPVariable, U> {
    T orderDomainValues(T domain, Assignment<R,U> assignment, CSP csp);
}
