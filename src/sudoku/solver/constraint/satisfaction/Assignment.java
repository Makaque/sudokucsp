package sudoku.solver.constraint.satisfaction;

/**
 * Created on 19/03/2018.
 */
public interface Assignment<R,L> {
    boolean isComplete();
    Assignment<R,L> add(R var, L value);
}
