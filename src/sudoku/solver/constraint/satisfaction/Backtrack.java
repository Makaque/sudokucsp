package sudoku.solver.constraint.satisfaction;

import java.util.Optional;

/**
 * Created on 19/03/2018.
 */
public abstract class Backtrack<A extends Assignment, C extends CSP> {

    public abstract Optional<A> backtrack(A assignment, C csp);
}
