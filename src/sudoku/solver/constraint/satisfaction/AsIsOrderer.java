package sudoku.solver.constraint.satisfaction;

/**
 * Created on 19/03/2018.
 */
public class AsIsOrderer<T extends CSPDomain, R extends CSPVariable, U> implements DomainOrderer<T,R,U> {

    @Override
    public T orderDomainValues(T domain, Assignment<R, U> assignment, CSP csp) {
        return domain;
    }
}
