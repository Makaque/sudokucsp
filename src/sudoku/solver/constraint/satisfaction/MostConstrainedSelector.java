package sudoku.solver.constraint.satisfaction;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

/**
 * Created on 19/03/2018.
 */
public class MostConstrainedSelector<T extends CSPVariable, U> implements VariableSelector<T, U> {
    private Comparator<T> smallestDomain
            = (t1, t2) -> Integer.compare(t1.getDomain().size(), t2.getDomain().size());

    @Override
    public Optional<T> selectUnassignedVariable(List<T> variables, Assignment<T, U> assignment) {
        try{
        return variables.stream().sorted(smallestDomain).filter(t -> t.getDomain().size() > 0).findFirst();

        } catch (Exception e){
            System.out.println(variables);
            System.out.println(assignment);
            throw e;
        }
    }

}
