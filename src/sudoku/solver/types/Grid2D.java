package sudoku.solver.types;

import sudoku.solver.utils.ListUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * Created on 19/03/2018.
 */
public class Grid2D<T> {
    public final int width;
    public final int height;
    final List<List<T>> grid;

    public Grid2D(int width, int height) {
        this.width = width;
        this.height = height;
        List<List<T>> grid = new ArrayList<>();
        for (int i = 0; i < width; i++) {
            List<T> col = new ArrayList<>(height);
            for (int j = 0; j < height; j++) {
                col.add(null);
            }
            grid.add(col);
        }
        this.grid = grid;
    }

    public Grid2D(int width, int height, List<List<T>> grid) {
        this.width = width;
        this.height = height;
        this.grid = ListUtils.copy2D(grid);
    }

    public Grid2D<T> fill(Supplier<T> values){
        int length = this.width * this.height;
        Grid2D<T> grid = new Grid2D<>(this.width, this.height);
        for (int i = 0; i < length; i++) {
            grid = grid.setPos(i, values.get());
        }
        return grid;
    }

    public Grid2D<T> fill(List<T> values){
        List<T> vals = ListUtils.copy(values);
        Supplier<T> s = () -> {
                T val = vals.get(0);
                vals.remove(0);
                return val;
        };
        return this.fill(s);
    }

    private Grid2D<T> copy() {
        List<List<T>> grid = ListUtils.copy2D(this.grid);
        return new Grid2D<>(this.width, this.height, grid);
    }

    private List<List<T>> setGridValue(int x, int y, T val) {
        List<List<T>> grid = ListUtils.copy2D(this.grid);
        grid.get(x)
                .set(y, val);
        return grid;
    }

    public Grid2D<T> set(int x, int y, T val) {
        List<List<T>> grid = setGridValue(x, y, val);
        return new Grid2D<>(this.width, this.height, grid);
    }

    public Grid2D<T> setPos(int pos, T val){
        int y = pos / this.width;
        int x = pos  % this.width;
        return set(x, y, val);
    }

    public Grid2D<T> setCol(int x, List<T> values) {
        Grid2D<T> newGrid = this.copy();
//        assert values.size() == height;
        this.grid.get(x).clear();
        this.grid.get(x).addAll(values);
        return newGrid;
    }

    public Grid2D<T> transpose(){
        return new Grid2D<T>(this.width, this.height, ListUtils.transpose(this.copy().grid));
    }

    public Grid2D<T> setRow(int y, List<T> values){
        Grid2D<T> newGrid = this.transpose();
        newGrid.setCol(y, values);
        return newGrid.transpose();
    }

    public int size() {
        return width * height;
    }

    public T get(int x, int y) {
        return grid.get(x).get(y);
    }

    public List<T> getRow(int y){
        return ListUtils.transpose(this.copy().grid).get(y);
    }

    public List<List<T>> getRows(){return ListUtils.transpose(this.copy().grid);}

    public List<T> getCol(int x){
        return this.copy().grid.get(x);
    }

    public T getAtPos(int pos){
        return this.toList().get(pos);
    }

    public List<T> toList() {
        List<List<T>> grid = ListUtils.transpose(this.copy().grid);
        List<T> list = new ArrayList<>();
        for (List<T> col : grid) {
            list.addAll(ListUtils.copy(col));
        }
        return list;
    }

    @Override
    public String toString() {
        List<List<T>> grid = ListUtils.transpose(this.copy().grid);
        String str = "";
        for (List<T> row : grid) {
            for (T elt : row) {
                str += elt + " ";
            }
            str = str.trim() + "\n";
        }
        return str.trim();
    }

}
