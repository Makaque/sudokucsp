package sudoku.solver;

import sudoku.solver.constraint.satisfaction.AsIsOrderer;
import sudoku.solver.constraint.satisfaction.DomainOrderer;
import sudoku.solver.constraint.satisfaction.MostConstrainedSelector;
import sudoku.solver.constraint.satisfaction.VariableSelector;
import sudoku.solver.game.SudokuDomain;
import sudoku.solver.game.SudokuVariable;
import sudoku.solver.io.IO;
import sudoku.solver.io.SudokuOuput;
import sudoku.solver.io.SudokuTextOutput;
import sudoku.solver.io.TextStreamIO;

public class Main {

    public static void main(String[] args) {
        VariableSelector<SudokuVariable, Integer> selector = new MostConstrainedSelector<>();
        DomainOrderer<SudokuDomain, SudokuVariable, Integer> orderer = new AsIsOrderer<>();
        SudokuOuput sudokuOuput = new SudokuTextOutput();
        IO io = new TextStreamIO(System.in, System.out, sudokuOuput);
        App cmdApp = new App();

        try{
            cmdApp.run(io, selector, orderer);
        } catch (Exception e){
            System.out.println("Unexpected exception.");
            System.out.println(e.getMessage());
            e.printStackTrace();
            System.out.println("Shutting down.");
        }
    }
}
