package sudoku.solver.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

/**
 * Created on 19/03/2018.
 */
public class ListUtils {
    public static <A> List<A> copy(List<A> list){
        return new ArrayList<>(list);
    }

    public static <A> List<List<A>> copy2D(List<List<A>> list){
        List<List<A>> cp = new ArrayList<>();
        for (List<A> l :
                list) {
            cp.add(copy(l));
        }
        return cp;
    }

    public static <T> List<T> fill(int length, Supplier<T> supplier){
        List<T> list = new ArrayList<>();
        for (int i = 0; i < length; i++) {
            list.add(supplier.get());
        }
        return list;
    }

    public static <T> List<List<T>> transpose(List<List<T>> table) {
        List<List<T>> ret = new ArrayList<List<T>>();
        final int N = table.get(0).size();
        for (int i = 0; i < N; i++) {
            List<T> col = new ArrayList<T>();
            for (List<T> row : table) {
                col.add(row.get(i));
            }
            ret.add(col);
        }
        return ret;
    }
}
