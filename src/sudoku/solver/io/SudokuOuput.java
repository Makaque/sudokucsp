package sudoku.solver.io;

import sudoku.solver.game.Sudoku;

/**
 * Created on 23/03/2018.
 */
public interface SudokuOuput {
    void outputSudoku(Sudoku sudoku);
}
