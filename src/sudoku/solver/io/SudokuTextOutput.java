package sudoku.solver.io;

import sudoku.solver.game.Sudoku;

import java.util.Collections;
import java.util.List;

/**
 * Created on 23/03/2018.
 */
public class SudokuTextOutput implements SudokuOuput {


    private int digitWidth(List<Integer> sudoku) {
        Integer max = Collections.max(sudoku);
        return max.toString().length();
    }

    private int digitWidth(Sudoku sudoku) {
        return digitWidth(sudoku.toList());
    }

    private String alignmentFormat(int width) {
        return "|%" + width + "s";
    }

    private String rowSeparatorGap(int width, String str){
        if(width == 0) return str;
        else return "-" + rowSeparatorGap(width - 1, str);
    }

    private String rowSeparatorGap(int width){
        return rowSeparatorGap(width, "");
    }

    private String addGaps(int dimensions, String gap){
        if(dimensions == 0) return gap;
        else return gap + addGaps(dimensions - 1, gap);
    }

    private String rowSeparator(int width, int dimensions){
        String separator = "+";
        String gap = rowSeparatorGap(width);
        return separator + addGaps(dimensions - 1, gap + separator);
    }

    private String sudokuEntry(int entry){
        if (entry == -1) return " ";
        else return String.valueOf(entry);
    }

    @Override
    public void outputSudoku(Sudoku sudoku) {
        int width = digitWidth(sudoku);
        int dimensions = sudoku.dimensions;
        String separator = rowSeparator(width, dimensions) + "%n";
        String format = alignmentFormat(width);
        List<List<Integer>> rows = sudoku.getRows();
        System.out.format(separator);
        for (List<Integer> row : rows) {
            for (Integer entry : row) {
                System.out.format(format, sudokuEntry(entry));
            }
            System.out.format("|%n");
            System.out.format(separator);
        }
    }
}
