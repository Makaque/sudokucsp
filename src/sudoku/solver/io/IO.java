package sudoku.solver.io;

import sudoku.solver.game.Sudoku;
import sudoku.solver.types.MultiPair;
import sudoku.solver.utils.IOMessage;

/**
 * Created on 12/02/2018.
 */
public interface IO {

    void displayDirections();

    IOMessage<Integer> promptBoardWidth();

    IOMessage<Sudoku> promptBoardValues(int blockWidth);

    void reportResult(MultiPair<Sudoku, Boolean> sudokuResult);

    void reportSolvedBoard(Sudoku board);

    void reportFailedBoard(Sudoku board);

    <A> void reportError(IOMessage.Error<A> error);

    void reportQuit();

}
