package sudoku.solver.io;

import sudoku.solver.game.Sudoku;
import sudoku.solver.types.MultiPair;
import sudoku.solver.utils.IOMessage;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created on 12/02/2018.
 */
public class TextStreamIO implements IO {
    private final InputStream input;
    private final PrintStream output;
    private final Scanner inputScanner;
    private final SudokuOuput sudokuOuput;


    public TextStreamIO(InputStream input, PrintStream output, SudokuOuput sudokuOuput) {
        this.input = input;
        this.output = output;
        this.inputScanner = new Scanner(input);
        this.sudokuOuput = sudokuOuput;
    }

    private void prompt(String out) {
        output.println(out);
    }

    private boolean isQuit(Scanner scanner) {
        return scanner.hasNext("[qQ]");
    }

    @Override
    public void displayDirections() {
        prompt("First enter the block width for the game. This is the width of the small block within the larger");
        prompt("game, or squre root of the width of the whole grid. For instance, in a 9x9 sudoku game, the block");
        prompt("width is 3.");
        prompt("Then enter all values for the sudoku game separated by whitespace, with '-' dashes representing");
        prompt("empty spaces.");
        prompt("Order your entries from top left box to bottom right box.");
        prompt("Enter Q any time to quit.");
    }

    private boolean isVal(Scanner scanner) {
        return (scanner.hasNextInt() || scanner.hasNext("-"));
    }

    private int nextVal(Scanner scanner) {
        if (scanner.hasNextInt()) {
            return scanner.nextInt();
        } else {
            scanner.next();
            return -1;
        }
    }

    private IOMessage<Sudoku> readBoard(Scanner scanner, int blockWidth) {
        try {
            List<Integer> sudokuList = new ArrayList<>();
            int length = blockWidth * blockWidth * blockWidth * blockWidth;
            for (int i = 0; i < length; i++) {
                if (isVal(scanner)) {
                    sudokuList.add(nextVal(scanner));
                } else if (isQuit(scanner)) {
                    scanner.next();
                    return new IOMessage.Quit<>();
                }
            }
            Sudoku sudoku = new Sudoku(blockWidth, sudokuList);
            return new IOMessage.Success<>(sudoku);
        } catch (Throwable t) {
            scanner.next();
            return new IOMessage.Error<>(new RuntimeException("Error reading board values", t));
        }
    }

    private IOMessage<Integer> readBlockWidth(Scanner scanner){
        if(scanner.hasNextInt()){
            return new IOMessage.Success<>(scanner.nextInt());
        } else if(isQuit(scanner)){
            scanner.next();
            return new IOMessage.Quit<>();
        }
        scanner.next();
        return new IOMessage.Error<>(new RuntimeException("Couldn't read block with"));
    }

    @Override
    public IOMessage<Integer> promptBoardWidth() {
        prompt("Block Width:");
        return readBlockWidth(inputScanner);
    }

    @Override
    public IOMessage<Sudoku> promptBoardValues(int blockWidth) {
        prompt("Board:");
        return readBoard(inputScanner, blockWidth);
    }

    @Override
    public void reportResult(MultiPair<Sudoku, Boolean> sudokuResult) {
        if (sudokuResult.right) {
            reportSolvedBoard(sudokuResult.left);
        } else {
            reportFailedBoard(sudokuResult.left);
        }
    }

    @Override
    public void reportSolvedBoard(Sudoku board) {
        sudokuOuput.outputSudoku(board);
        prompt("Solved!");
    }

    @Override
    public void reportFailedBoard(Sudoku board) {
        sudokuOuput.outputSudoku(board);
        prompt("Could not be solved");
    }

    @Override
    public <A> void reportError(IOMessage.Error<A> error) {
        prompt("Encountered an error:");
        prompt(error.t.getMessage());
    }

    @Override
    public void reportQuit() {
        prompt("Shutting down");
    }
}
