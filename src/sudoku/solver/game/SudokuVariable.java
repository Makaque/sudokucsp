package sudoku.solver.game;

import sudoku.solver.constraint.satisfaction.CSPVariable;

/**
 * Created on 19/03/2018.
 */
public class SudokuVariable implements CSPVariable{
    public final int xPos;
    public final int yPos;
    private final int dimensions;
    public final int pos;

    public final SudokuDomain domain;

    @Override
    public String toString() {
        return "Var(" + xPos + "," + yPos + ") " + domain;
    }

    public SudokuVariable(int xPos, int yPos, int dimensions, SudokuDomain domain) {
        this.xPos = xPos;
        this.yPos = yPos;
        this.dimensions = dimensions;
        this.domain = domain;
        this.pos = xPos + dimensions * yPos;
    }

    @Override
    public SudokuDomain getDomain() {
        return domain;
    }
}
