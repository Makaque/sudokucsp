package sudoku.solver.game;

import sudoku.solver.constraint.satisfaction.Backtrack;

import java.util.Optional;

public class SudokuBacktrack extends Backtrack<Sudoku, SudokuCSP> {

    @Override
    public Optional<Sudoku> backtrack(Sudoku sudoku, SudokuCSP csp)
    {
        if (sudoku.isComplete()) {
//            System.out.println("completed");
            return Optional.of(sudoku);
        }
        Optional<SudokuVariable> maybeVar = csp.selectUnassignedVariable(sudoku);
        SudokuVariable var = null;
        if(maybeVar.isPresent()){
            var = maybeVar.get();
        } else {
            return Optional.empty();
        }

        for (Integer value : csp.orderDomainValues(var, sudoku)) {
//            System.out.println(var + " " + value);
            if (csp.consistent(var, value, sudoku)) {
//            System.out.println("consistent");
                Sudoku newSudoku = sudoku.add(var, value);
                SudokuCSP newCSP = csp.apply(var, value, sudoku);
                Optional<Sudoku> maybeSudoku = backtrack(newSudoku, newCSP);
                if (maybeSudoku.isPresent()) {
                    return maybeSudoku;
                }
            }
        }
//        System.out.println(sudoku.board.toString());
        return Optional.empty();
    }
}
