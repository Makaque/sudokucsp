package sudoku.solver.game;

import sudoku.solver.constraint.satisfaction.Assignment;
import sudoku.solver.types.Grid2D;
import sudoku.solver.types.MultiPair;
import sudoku.solver.utils.ListUtils;

import java.util.ArrayList;
import java.util.List;

public class Sudoku implements Assignment<SudokuVariable, Integer> {
    public final int blockWidth;
    public final int dimensions;
    final Grid2D<Integer> blocks;
    final Grid2D<Integer> board;
    private final List<Integer> blockOfPos;

    private static MultiPair<Grid2D<Integer>, List<Integer>> initBlocks(int blockWidth, int dimensions) {
        List<Integer> blockOfPos = ListUtils.fill(dimensions * dimensions, () -> null);
        List<List<Integer>> blockList = new ArrayList<>();
        for (int i = 0; i < dimensions; i++) {
            List<Integer> block = new ArrayList<>();
            for (int j = 0; j < blockWidth; j++) {
                List<Integer> blockRow = new ArrayList<>();
                for (int k = 0; k < blockWidth; k++) {
                    int value =
                            ((i * blockWidth) % dimensions)
                                    + (dimensions * blockWidth * ((int) (i / blockWidth)))
                                    + (dimensions * j)
                                    + k;
                    blockRow.add(value);
                }
                block.addAll(blockRow);
            }
            blockList.add(block);
        }
        for (int i = 0; i < blockList.size(); i++) {
            List<Integer> block = blockList.get(i);
            for (int j = 0; j < block.size(); j++) {
                blockOfPos.set(block.get(j), i);
            }
        }
        Grid2D<Integer> grid = new Grid2D<>(dimensions, dimensions, blockList).transpose();
        return new MultiPair<>(grid, blockOfPos);
    }


    private Sudoku(int blockWidth, Grid2D<Integer> board) {
        this.blockWidth = blockWidth;
        this.dimensions = blockWidth * blockWidth;
        this.board = board;
        MultiPair<Grid2D<Integer>, List<Integer>> blockVals = initBlocks(blockWidth, dimensions);
        this.blocks = blockVals.left;
        this.blockOfPos = blockVals.right;
    }

    private Sudoku(int blockWidth, int dimensions, List<Integer> initial) {
        this(blockWidth, new Grid2D<Integer>(dimensions, dimensions).fill(initial));
    }

    public Sudoku(int blockWidth, List<Integer> initial) {
        this(blockWidth, blockWidth * blockWidth, initial);
    }

    public Sudoku set(int x, int y, int val) {
        Grid2D<Integer> board = this.board.set(x, y, val);
        return new Sudoku(blockWidth, board);
    }


    public Integer get(int x, int y) {
        return board.get(x, y);
    }

    public Integer blockOfPosition(int pos){
        return blockOfPos.get(pos);
    }

    public boolean inBlock(int blockIndex, Integer value){
        List<Integer> block = blocks.getRow(blockIndex);
        return block.stream().anyMatch(v -> board.getAtPos(v).equals(value));
    }

    public boolean inRow(int y, Integer value){
        return board.getRow(y).contains(value);
    }

    public boolean inCol(int x, Integer value){
        return board.getCol(x).contains(value);
    }

    public boolean isSet(int x, int y){
        return get(x, y) != -1;
    }

    public List<Integer> toList(){
        return board.toList();
    }

    public List<List<Integer>> getRows(){
        return board.getRows();
    }

    @Override
    public boolean isComplete() {
        return board.toList().stream().noneMatch(val -> val == -1);
    }

    @Override
    public Sudoku add(SudokuVariable var, Integer value) {
        return set(var.xPos, var.yPos, value);
    }

    @Override
    public String toString() {
        return board.toString();
    }
}
