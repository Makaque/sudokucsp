package sudoku.solver.game;

import sudoku.solver.constraint.satisfaction.CSPDomain;
import sudoku.solver.utils.ListUtils;

import java.util.List;

/**
 * Created on 19/03/2018.
 */
public class SudokuDomain implements CSPDomain<Integer>{
    private final List<Integer> domain;

    @Override
    public String toString() {
        String str = "Domain: {";
        String list = "";
        for (Integer elt : domain){
            list += elt + " ";
        }
        str += list.trim().replaceAll(" ", ",") + "}";
        return str;
    }

    public SudokuDomain(List<Integer> domain) {
        this.domain = domain;
    }

    @Override
    public int size() {
        return domain.size();
    }

    @Override
    public List<Integer> getValues() {
        return ListUtils.copy(domain);
    }
}
