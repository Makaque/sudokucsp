package sudoku.solver.game;

import sudoku.solver.constraint.satisfaction.CSP;
import sudoku.solver.constraint.satisfaction.DomainOrderer;
import sudoku.solver.constraint.satisfaction.VariableSelector;
import sudoku.solver.utils.ListUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

public class SudokuCSP implements CSP<SudokuVariable, Integer, Sudoku> {
    private final List<SudokuVariable> variables;
    private final VariableSelector<SudokuVariable, Integer> selector;
    private final DomainOrderer<SudokuDomain, SudokuVariable, Integer> orderer;


    @Override
    public String toString() {
        String str = "SudokuCSP:\n";
        str += " | Variables:\n";
        for (SudokuVariable var : variables){
            str += " |   " + var + "\n";
        }
        str += " | Constraints:\n";
        str += " |   alldiff(row)\n";
        str += " |   alldiff(col)\n";
        str += " |   alldiff(box)\n";
        return str;
    }

    private static List<SudokuVariable> initVariables(Sudoku sudoku){
        List<SudokuVariable> sudokuVariables = new ArrayList<>();
        for (int i = 0; i < sudoku.dimensions; i++) {
            for (int j = 0; j < sudoku.dimensions; j++) {
                List<Integer> domain = new ArrayList<>();
                for (int k = 1; k <= sudoku.dimensions; k++) {
                    if(satisfiesConstraints(new SudokuVariable(j, i, sudoku.dimensions, null), k, sudoku)){
                        domain.add(k);
                    }
                }
                SudokuVariable variable = new SudokuVariable(j, i, sudoku.dimensions, new SudokuDomain(domain));
                sudokuVariables.add(variable);
            }
        }
        return sudokuVariables;
    }

    private List<SudokuVariable> updateVariables(Sudoku sudoku){
        return variables.stream().map(v -> {
            List<Integer> newDomain = v.domain.getValues().stream().filter(d -> satisfiesConstraints(v, d, sudoku)).collect(Collectors.toList());
            SudokuDomain newSudokuDomain = new SudokuDomain(newDomain);
            return new SudokuVariable(v.xPos, v.yPos, sudoku.dimensions, newSudokuDomain);
        }).collect(Collectors.toList());
    }

    public SudokuCSP(VariableSelector<SudokuVariable, Integer> selector,
                     DomainOrderer<SudokuDomain, SudokuVariable, Integer> orderer, Sudoku sudoku) {
        this.variables = initVariables(sudoku);
        this.selector = selector;
        this.orderer = orderer;
    }

    private SudokuCSP(VariableSelector<SudokuVariable, Integer> selector,
                      DomainOrderer<SudokuDomain, SudokuVariable, Integer> orderer, List<SudokuVariable> variables){
        this.variables = ListUtils.copy(variables);
        this.selector = selector;
        this.orderer = orderer;
    }

    @Override
    public Optional<SudokuVariable> selectUnassignedVariable(Sudoku assignment) {
        return selector.selectUnassignedVariable(this.variables, assignment);
    }

    @Override
    public List<Integer> orderDomainValues(SudokuVariable var, Sudoku assignment) {
        return orderer.orderDomainValues(var.domain, assignment, this).getValues();
    }

    private static Function<SudokuVariable, Function<Integer, Function<Sudoku, Boolean>>> alldiffBox =
            var -> value -> sudoku -> {
                Integer blockIndex = sudoku.blockOfPosition(var.pos);
                return !sudoku.inBlock(blockIndex, value);
            };

    private static Function<SudokuVariable, Function<Integer, Function<Sudoku, Boolean>>> alldiffRow =
            var -> value -> sudoku ->
                    !sudoku.inRow(var.yPos, value);

    private static Function<SudokuVariable, Function<Integer, Function<Sudoku, Boolean>>> allDiffCol =
            var -> value -> sudoku ->
                    !sudoku.inCol(var.xPos, value);

    private static Function<SudokuVariable, Function<Sudoku, Boolean>> notSet =
            var -> sudoku ->
                    !sudoku.isSet(var.xPos, var.yPos);

    public static boolean satisfiesConstraints(SudokuVariable var, Integer value, Sudoku sudoku) {
        return
        notSet.apply(var).apply(sudoku)
        // Value doesn't exist in small block
        && alldiffBox.apply(var).apply(value).apply(sudoku)
        // Value doesn't exist in row
        && alldiffRow.apply(var).apply(value).apply(sudoku)
        // Value doesn't exist in col
        && allDiffCol.apply(var).apply(value).apply(sudoku);
    }

    @Override
    public boolean consistent(SudokuVariable var, Integer value, Sudoku sudoku) {
        return satisfiesConstraints(var, value, sudoku);
    }

    @Override
    public SudokuCSP apply(SudokuVariable var, Integer value, Sudoku sudoku) {
//        return  new SudokuCSP(this.selector, this.orderer, sudoku.set(var.xPos, var.yPos, value));
        List<SudokuVariable> newVars =  updateVariables(sudoku.set(var.xPos, var.yPos, value));
        return new SudokuCSP(this.selector, this.orderer, newVars);
    }
}
