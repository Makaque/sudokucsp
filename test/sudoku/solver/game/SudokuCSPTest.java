package sudoku.solver.game;

import org.junit.jupiter.api.Test;
import sudoku.solver.constraint.satisfaction.AsIsOrderer;
import sudoku.solver.constraint.satisfaction.MostConstrainedSelector;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class SudokuCSPTest {

    private Sudoku getSudoku1(){
        return new Sudoku(
                3,
                Arrays.asList(
                        5, 3,-1,-1, 7,-1,-1,-1,-1,
                        6,-1,-1, 1, 9, 5,-1,-1,-1,
                        -1, 9, 8,-1,-1,-1,-1, 6,-1,
                        8,-1,-1,-1, 6,-1,-1,-1, 3,
                        4,-1,-1, 8,-1, 3,-1,-1, 1,
                        7,-1,-1,-1, 2,-1,-1,-1, 6,
                        -1, 6,-1,-1,-1,-1, 2, 8,-1,
                        -1,-1,-1, 4, 1, 9,-1,-1, 5,
                        -1,-1,-1,-1, 8,-1,-1, 7, 9
                )
        );
    }

    private Sudoku getSudoku2(){
        return new Sudoku(
                3,
                Arrays.asList(
                        5, 3, 4, 6, 7, 8, 9, 1, 2,
                        6, 7, 2, 1, 9, 5, 3, 4, 8,
                        1, 9, 8, 3, 4, 2, 5, 6, 7,
                        8, 5, 9, 7, 6, 1, 4, 2, 3,
                        4, 2, 6, 8, 5, 3, 7, 9, 1,
                        7, 1, 3, 9, 2, 4, 8, 5, 6,
                        9, 6, 1, 5, 3, 7, 2, 8, 4,
                        2, 8, 7, 4, 1, 9, 6, 3, 5,
                        3, 4, 5, 2, 8, 6,-1, 7, 9
                )
        );
    }

    @Test
    void constructor(){
        Sudoku sudoku = new Sudoku(
                3,
                Arrays.asList(
                        5, 3,-1,-1, 7,-1,-1,-1,-1,
                        6,-1,-1, 1, 9, 5,-1,-1,-1,
                        -1, 9, 8,-1,-1,-1,-1, 6,-1,
                        8,-1,-1,-1, 6,-1,-1,-1, 3,
                        4,-1,-1, 8,-1, 3,-1,-1, 1,
                        7,-1,-1,-1, 2,-1,-1,-1, 6,
                        -1, 6,-1,-1,-1,-1, 2, 8,-1,
                        -1,-1,-1, 4, 1, 9,-1,-1, 5,
                        -1,-1,-1,-1, 8,-1,-1, 7, 9
                )
        );

        SudokuCSP sudokuCSP = new SudokuCSP(new MostConstrainedSelector<>(), new AsIsOrderer<>(), sudoku);
        System.out.println(sudokuCSP);
    }

    @Test
    void selectUnassignedVariable() {
    }

    @Test
    void orderDomainValues() {
    }

    @Test
    void satisfiesConstraints() {
        Sudoku sudoku = getSudoku1();
        boolean b = SudokuCSP.satisfiesConstraints(new SudokuVariable(0, 0, 9, null), 2, sudoku);
        if(!b){
            fail("2 is a valid choice for the first square");
        }
    }

    @Test
    void satisfiesConstraints2() {
        Sudoku sudoku = getSudoku2();
        boolean b = SudokuCSP.satisfiesConstraints(new SudokuVariable(6, 8, 9, null), 1, sudoku);
        if(!b){
            fail("1 is a valid choice for the final");
        }
    }

    @Test
    void consistent() {
    }

    @Test
    void apply() {
    }
}