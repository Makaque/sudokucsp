package sudoku.solver.game;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created on 20/03/2018.
 */
class SudokuTest {

    private Sudoku getSudoku1(){
        return new Sudoku(
                3,
                Arrays.asList(
                        5, 3,-1,-1, 7,-1,-1,-1,-1,
                        6,-1,-1, 1, 9, 5,-1,-1,-1,
                        -1, 9, 8,-1,-1,-1,-1, 6,-1,
                        8,-1,-1,-1, 6,-1,-1,-1, 3,
                        4,-1,-1, 8,-1, 3,-1,-1, 1,
                        7,-1,-1,-1, 2,-1,-1,-1, 6,
                        -1, 6,-1,-1,-1,-1, 2, 8,-1,
                        -1,-1,-1, 4, 1, 9,-1,-1, 5,
                        -1,-1,-1,-1, 8,-1,-1, 7, 9
                )
        );
    }

    @Test
    void constructor(){
        Sudoku sudoku = new Sudoku(
                3,
                Arrays.asList(
                         5, 3,-1,-1, 7,-1,-1,-1,-1,
                         6,-1,-1, 1, 9, 5,-1,-1,-1,
                        -1, 9, 8,-1,-1,-1,-1, 6,-1,
                         8,-1,-1,-1, 6,-1,-1,-1, 3,
                         4,-1,-1, 8,-1, 3,-1,-1, 1,
                         7,-1,-1,-1, 2,-1,-1,-1, 6,
                        -1, 6,-1,-1,-1,-1, 2, 8,-1,
                        -1,-1,-1, 4, 1, 9,-1,-1, 5,
                        -1,-1,-1,-1, 8,-1,-1, 7, 9
                )
        );
        System.out.println(sudoku.board);
    }

    @Test
    void set() {
    }

    @Test
    void get() {
    }

    @Test
    void blockOfPosition() {
    }

    @Test
    void inBlock() {
        Sudoku sudoku = getSudoku1();
        if(sudoku.inBlock(0, 7)){
            fail("7 is not in the first block");
        }
        if(!sudoku.inBlock(0, 5)){
            fail("5 is in the first block");
        }
    }

    @Test
    void inBlock2() {
        Sudoku sudoku = getSudoku1();
        if(sudoku.inBlock(0, 2)){
            fail("2 is not in the first block");
        }
    }

    @Test
    void inRow() {
        Sudoku sudoku = getSudoku1();
        if(sudoku.inRow(1, 7)){
            fail("7 is not in the second row");
        }
        if(!sudoku.inRow(1, 6)){
            fail("6 is in the second row");
        }
    }

    @Test
    void inCol() {
        Sudoku sudoku = getSudoku1();
        if(sudoku.inCol(1, 7)){
            fail("7 is not in the second column");
        }
        if(!sudoku.inCol(1, 6)){
            fail("6 is in the second column");
        }
    }

    @Test
    void isComplete() {
    }

    @Test
    void add() {
    }

}