package sudoku.solver.constraint.satisfaction;

import org.junit.jupiter.api.Test;
import sudoku.solver.game.Sudoku;
import sudoku.solver.game.SudokuBacktrack;
import sudoku.solver.game.SudokuCSP;

import java.util.Arrays;
import java.util.Optional;

/**
 * Created on 20/03/2018.
 */
class BacktrackTest {
        @Test
        void backtrack() {
            Sudoku sudoku = new Sudoku(
                    3,
                    Arrays.asList(
                            5, 3,-1,-1, 7,-1,-1,-1,-1,
                            6,-1,-1, 1, 9, 5,-1,-1,-1,
                            -1, 9, 8,-1,-1,-1,-1, 6,-1,
                            8,-1,-1,-1, 6,-1,-1,-1, 3,
                            4,-1,-1, 8,-1, 3,-1,-1, 1,
                            7,-1,-1,-1, 2,-1,-1,-1, 6,
                            -1, 6,-1,-1,-1,-1, 2, 8,-1,
                            -1,-1,-1, 4, 1, 9,-1,-1, 5,
                            -1,-1,-1,-1, 8,-1,-1, 7, 9
                    )
            );

            SudokuCSP sudokuCSP = new SudokuCSP(new MostConstrainedSelector<>(), new AsIsOrderer<>(), sudoku);
            SudokuBacktrack backtracker = new SudokuBacktrack();
            Optional<Sudoku> solved = backtracker.backtrack(sudoku, sudokuCSP);
            solved.ifPresent(s -> System.out.println(solved.get()));
        }

}