package sudoku.solver.types;

import org.junit.jupiter.api.Test;
import sudoku.solver.types.Grid2D;
import sudoku.solver.utils.ListUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created on 20/03/2018.
 */
class Grid2DTest {
    @Test
    void fill() {
        List<Integer> list = Arrays.asList(1, 2, 3, 4);
        Grid2D<Integer> grid = new Grid2D<>(2,2);
        grid = grid.fill(list);
        System.out.println(grid);
    }

    @Test
    void fill1() {
    }

    @Test
    void setPos() {
    }

    @Test
    void getCol() {
        List<Integer> list = Arrays.asList(1, 2, 3, 4);
        Grid2D<Integer> grid = new Grid2D<>(2,2);
        grid = grid.fill(list);
        System.out.println(grid.getCol(0));
    }

    @Test
    void set() {
        Grid2D<Integer> grid = new Grid2D<>(2, 2);
//        System.out.println(grid);
        grid = grid.set(0, 0, 0);
        grid = grid.set(1, 0, 1);
        grid = grid.set(0, 1, 2);
        grid = grid.set(1, 1, 3);

        System.out.println(grid);
    }

    @Test
    void setCol() {
    }

    @Test
    void size() {
    }

    @Test
    void get() {
    }

    @Test
    void getRow() {
    }

    @Test
    void toList() {
    }

    @Test
    void testToString() {
    }

}